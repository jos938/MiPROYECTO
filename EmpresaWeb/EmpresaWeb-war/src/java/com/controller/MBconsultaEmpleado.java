/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.controller;


import com.modelo.ConexionDB;
import com.modelo.EmpleadoDao;
import com.modelo.MyBean;
import com.modelo.MyBeanLocal;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import javax.ejb.EJB;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
@ManagedBean(name = "mbconsulta")
@RequestScoped
public class MBconsultaEmpleado implements Serializable{
    @EJB
    public MyBeanLocal myBean;
    EmpleadoDao miEmpleado = new EmpleadoDao();
    
    
    
    EmpleadoDao miempleCosultado = new EmpleadoDao();
    
    
    static Connection mysqlcon=null;
    static ResultSet rs=null;
    static Statement st=null;
    static String  query=null;
    private List<String> NombreDepartamentos;
    private int DocEmpleado;
    static int DocEmpleado1;
    private int CodEmpleado;
    private int codEmpleado1;
    static String nombre;
    private String nombre1;
    static String apellido;
    private String apellido1;
    static String direccion;
    private String direccion1;
    static String telefono;
    private String telefono1;
    static String JefeInmediato;
    private String JefeInmediato1;
    static int idDepartamento;
    private int idDepartamento1;
    static int idContrato;
    private int idContrato1;
    static int docConsulta=0;
    int cont =0;
    
    
    public EmpleadoDao getMiempleCosultado() {
        return miempleCosultado;
    }
    
    public void setMiempleCosultado(EmpleadoDao miempleCosultado) {
        this.miempleCosultado = miempleCosultado;
    }
    public EmpleadoDao getMiEmpleado() {
        return miEmpleado;
    }
    
    public void setMiEmpleado(EmpleadoDao miEmpleado) {
        this.miEmpleado = miEmpleado;
    }
    
    public int getDocConsulta() {
        return docConsulta;
    }
    
    public void setDocConsulta(int docConsulta) {
        this.docConsulta = docConsulta;
    }
    
    
    
    
    private String NombreDepartamento=null;
    
    public  int getDocEmpleado1() {
        return DocEmpleado1;
    }
    
    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }
    
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    
    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }
    
    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }
    
    public void setJefeInmediato1(String JefeInmediato1) {
        this.JefeInmediato1 = JefeInmediato1;
    }
    
    public void setIdDepartamento1(int idDepartamento1) {
        this.idDepartamento1 = idDepartamento1;
    }
    
    public void setIdContrato1(int idContrato1) {
        this.idContrato1 = idContrato1;
    }
    
    public void setDocEmpleado1(int DocEmpleado1) {
        this.DocEmpleado1 = DocEmpleado1;
    }
    
    /*public static void setDocEmpleado1(int DocEmpleado1) {
    MBconsultaEmpleado.DocEmpleado1 = DocEmpleado1;
    }*/
    
    public int getCodEmpleado1() {
        return codEmpleado1;
    }
    
    public void setCodEmpleado1(int codEmpleado1) {
        this.codEmpleado1 = codEmpleado1;
    }
    
    public String getNombre1() {
        return nombre1;
    }
    
    /*public static void setNombre1(String nombre1) {
    MBconsultaEmpleado.nombre1 = nombre1;
    }*/
    
    public String getApellido1() {
        return apellido1;
    }
    
    /*public static void setApellido1(String apellido1) {
    MBconsultaEmpleado.apellido1 = apellido1;
    }*/
    
    public String getDireccion1() {
        return direccion1;
    }
    
    /*public static void setDireccion1(String direccion1) {
    MBconsultaEmpleado.direccion1 = direccion1;
    }*/
    
    public String getTelefono1() {
        return telefono1;
    }
    
    /*public static void setTelefono1(String telefono1) {
    MBconsultaEmpleado.telefono1 = telefono1;
    }*/
    
    public String getJefeInmediato1() {
        return JefeInmediato1;
    }
    
    /*public static void setJefeInmediato1(String JefeInmediato1) {
    MBconsultaEmpleado.JefeInmediato1 = JefeInmediato1;
    }*/
    
    public int getIdDepartamento1() {
        return idDepartamento1;
    }
    
    /* public static void setIdDepartamento1(int idDepartamento1) {
    MBconsultaEmpleado.idDepartamento1 = idDepartamento1;
    }*/
    
    
    public int getIdContrato1() {
        return idContrato1;
    }
    
    /*public static void setIdContrato1(int idContrato1) {
    MBconsultaEmpleado.idContrato1 = idContrato1;
    }*/
    
    
    
    public int getDocEmpleado() {
        
        return DocEmpleado;
    }
    public void setDocEmpleado(int docEmpleado) {
        DocEmpleado = docEmpleado;
    }
    public int getCodEmpleado() {
        return CodEmpleado;
    }
    public void setCodEmpleado(int CodEmpleado) {
        this.CodEmpleado = CodEmpleado;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getJefeInmediato() {
        return JefeInmediato;
    }
    public void setJefeInmediato(String jefeInmediato) {
        this.JefeInmediato = jefeInmediato;
    }
    public int getIdDepartamento() {
        return idDepartamento;
    }
    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    public int getIdContrato() {
        return idContrato;
    }
    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }
    
    
    public  MBconsultaEmpleado() {
        
    }
    
    public int getCont() {
        return cont;
    }
    
    public void setCont(int cont) {
        this.cont = cont;
    }
    
    public String LimpiarInic(){
        setDocEmpleado(0);
        setNombre("");
        setApellido("");
        setDireccion("");
        setTelefono("");
        setJefeInmediato("");
        setIdDepartamento(0);
        setIdContrato(0);
        return "index.xhtml";
    }
    
    
    public void registrarEmpleado(){
        myBean = new MyBean();
        EmpleadoDao miemple = new EmpleadoDao();
        miemple.setDocEmpleado1(getDocEmpleado1());
        miemple.setNombre1(getNombre1());
        miemple.setApellido1(getApellido1());
        miemple.setDireccion1(getDireccion1());
        miemple.setTelefono1(getTelefono1());
        miemple.setJefeInmediato1(getJefeInmediato1());
        miemple.setIdDepartamento1(getIdDepartamento1());
        miemple.setIdContrato1(getIdContrato1());
        
        myBean.registrarEmple(miemple);
        
    }
    
    /*   public String getNombreDepartamento() {
    return NombreDepartamento;
    }
    public void setNombreDepartamento(String NombreDepartamento) {
    this.NombreDepartamento = NombreDepartamento;
    }
    
    public List<String> NombreDepartamento() {
    try {
    
    List<String> NombreDepartamentos= new ArrayList<String>();
    query="select NombreDepartamento from departamento";
    mysqlcon= ConexionDB.Conexion();
    st =  mysqlcon.createStatement();
    rs = st.executeQuery(query);
    while (rs.next()) {
    NombreDepartamentos.add(rs.getString(2));
    }
    mysqlcon.close();
    
    return NombreDepartamentos;
    } catch (SQLException e) {
    JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta"+e);
    return null;
    }
    
    }*/
    
    
    
    public String consultar(){
        
        
        myBean = new MyBean();
        miEmpleado=myBean.consultar(docConsulta);
        cont = 1;
        return "index.xhtml";
    }
    
    
    public String eliminar(){
        
        try {
            
            String sql = "delete from empleados where DocEmpleado='"+getDocConsulta()+"'";
            PreparedStatement ps = ConexionDB.Connection().prepareStatement(sql);
            int rowdelete = ps.executeUpdate();
            if (rowdelete>0) {
                JOptionPane.showMessageDialog(null, "El empleado Se ha Eliminado Satisfactoriamente..!!");
            }if(rowdelete==0){
                JOptionPane.showMessageDialog(null, "NO EXISTE LA IDENTIFICACION ingresada "+docConsulta+" en la base de datos.",
                        "ERROR",JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al eliminar "+e );
        }
        return "index.xhtml";
    }
    
    public String modificaEmpleado(){
         myBean = new MyBean();
 
        EmpleadoDao miEmpleM= new EmpleadoDao();
      
       
        miEmpleM.setDocEmpleado1(getDocConsulta());
         JOptionPane.showMessageDialog(null, "doc a mod"+docConsulta);
        miEmpleM.setNombre1(miempleCosultado.getNombre1());
        miEmpleM.setApellido1(miempleCosultado.getApellido1());
        miEmpleM.setDireccion1(miempleCosultado.getDireccion1());
        JOptionPane.showMessageDialog(null,"dir" +miempleCosultado.getDireccion1());
        miEmpleM.setTelefono1(miempleCosultado.getTelefono1());
        miEmpleM.setJefeInmediato1(miempleCosultado.getJefeInmediato1());
        miEmpleM.setIdDepartamento1(miempleCosultado.getIdDepartamento1());
        miEmpleM.setIdContrato1(miempleCosultado.getIdContrato1());
         myBean.modificarEmple(miEmpleM);
       return "index.xhtml";
    }
    public void pagMod(){
      myBean = new MyBean();
        
        JOptionPane.showMessageDialog(null,"doc" +docConsulta);
        EmpleadoDao miEmpleM= new EmpleadoDao();
        miempleCosultado = myBean.consultar(docConsulta);
 
    }
}




