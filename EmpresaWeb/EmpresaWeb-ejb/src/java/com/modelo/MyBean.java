/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.modelo;

import java.sql.Statement;
import javax.ejb.Stateless;
import javax.swing.JOptionPane;
import com.modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *https://www.youtube.com/watch?v=zBPPERzmOVE
 * @author Administrator
 */
@Stateless
public class MyBean implements MyBeanLocal {
    
    
    public void registrarEmple(EmpleadoDao miemple) {
        try {
            Statement statement = ConexionDB.Connection().createStatement();
            String  query = "insert into empleados values('"+miemple.getDocEmpleado1()+"','"+miemple.getCodEmpleado1()+"','"+miemple.getNombre1()+"','"+miemple.getApellido1()+"','"+miemple.getDireccion1()+"','"+miemple.getTelefono1()+"','"+miemple.getJefeInmediato1()+"','"+miemple.getIdDepartamento1()+"','"+miemple.getIdContrato1()+"')";
            statement.executeUpdate(query);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Los Datos NO se han insertado "+ e);
        }
        
    }
    
    public EmpleadoDao consultar(int  doc){
        EmpleadoDao miemple = new EmpleadoDao();
        Connection mysqlcon=null;
        ResultSet rs=null;
        Statement st=null;
        String  query=null;
        int cont=0;
        try{
            query = "select * from empleados where DocEmpleado='"+doc+"'";
            
            mysqlcon= ConexionDB.Connection();
            st =  mysqlcon.createStatement();
            rs = st.executeQuery(query);
            
            while (rs.next()) {
                cont = cont + 1;
                
                miemple.setDocEmpleado1((rs.getInt(1)));
                miemple.setNombre1(rs.getString(3));
                miemple.setApellido1(rs.getString(4));
                miemple.setDireccion1(rs.getString(5));
                miemple.setTelefono1(rs.getString(6));
                miemple.setJefeInmediato1(rs.getString(7));
                miemple.setIdDepartamento1((rs.getInt(8)));
                miemple.setIdContrato1(Integer.parseInt(rs.getString(9)));
                miemple.getNombre1();
            }mysqlcon.close();
        }catch (Exception e) {
            System.out.println( "No se pudo realizar la consulta"+e);
        }
        return miemple;
    }
    
    
    public  String  modificarEmple(EmpleadoDao miEmpleM){
         
        try {
            String query="update empleados set nombre=?,apellido=?,direccion=?,telefono=?,JefeInmediato=?,idDepartamento=?,idContrato=? where DocEmpleado=? ";
            PreparedStatement ps = ConexionDB.Connection().prepareStatement(query);
          //  ps.setInt(1,empleado.getNombre1());
            ps.setString(1,miEmpleM.getNombre1());
            ps.setString(2,miEmpleM.getApellido1());
            ps.setString(3,miEmpleM.getDireccion1());
             
            ps.setString(4,miEmpleM.getTelefono1());
            ps.setString(5,miEmpleM.getJefeInmediato1());
            ps.setInt(6,miEmpleM.getIdDepartamento1());
            ps.setInt(7,miEmpleM.getIdContrato1());
            ps.setInt(8,miEmpleM.getDocEmpleado1());
             
             JOptionPane.showMessageDialog(null,"query"+ ps);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "El empleado se ha MODIFICADO correctamente");
       return "index.xhml";
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Los Datos NO se han ACTUALIZADO "+ e);
        }
        return null;
    }
}


