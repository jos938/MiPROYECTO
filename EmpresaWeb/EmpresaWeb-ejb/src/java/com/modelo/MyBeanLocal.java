/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.modelo;

import java.sql.Statement;
import javax.ejb.Local;
import javax.swing.JOptionPane;
import com.modelo.*;
/**
 *
 * @author Administrator
 */
@Local
public interface MyBeanLocal {
    
    /**
     *
     * @param miemple
     */
    
    public void registrarEmple(EmpleadoDao miemple);
    public EmpleadoDao consultar(int  doc);
    public  String  modificarEmple(EmpleadoDao miEmpleM);
}
