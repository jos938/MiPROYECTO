
package com.empresaWeb.empresaWeb.ejb;


/**
 *
 * @author Administrator
 */
public class EmpleadoDao {
    
    private int  DocEmpleado1;
    private int CodEmpleado1;
    private String nombre1;
    private String apellido1;
    private String direccion1;
    private String telefono1;
    private String jefeInmediato1;
    private int idDepartamento1;
    private int idContrato1;
    private int docConsulta;

    public  int getDocConsulta() {
        return docConsulta;
    }

    public  void setDocConsulta(int docConsulta) {
       this.docConsulta = docConsulta;
    }

    public int getDocEmpleado1() {
        return DocEmpleado1;
    }

    public void setDocEmpleado1(int DocEmpleado1) {
        this.DocEmpleado1 = DocEmpleado1;
    }

    public int getCodEmpleado1() {
        return CodEmpleado1;
    }

    public void setCodEmpleado1(int CodEmpleado1) {
        this.CodEmpleado1 = CodEmpleado1;
    }

    public String getNombre1() {
       return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getJefeInmediato1() {
        return jefeInmediato1;
    }

    public void setJefeInmediato1(String jefeInmediato1) {
        this.jefeInmediato1 = jefeInmediato1;
    }

    public int getIdDepartamento1() {
        return idDepartamento1;
    }

    public void setIdDepartamento1(int idDepartamento1) {
        this.idDepartamento1 = idDepartamento1;
    }

    public int getIdContrato1() {
        return idContrato1;
    }

    public void setIdContrato1(int idContrato1) {
        this.idContrato1 = idContrato1;
    }
    
    
}

