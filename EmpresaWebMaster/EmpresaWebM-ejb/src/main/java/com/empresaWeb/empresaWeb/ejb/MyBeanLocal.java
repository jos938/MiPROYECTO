/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.empresaWeb.empresaWeb.ejb;

import javax.ejb.Remote;

/**
 *
 * @author Administrator
 */
@Remote
public interface MyBeanLocal {
    
    /**
     *
     * @param miemple
     */
    
    public void aregistrarEmple(EmpleadoDao miemple);
    public EmpleadoDao bconsultar(int  doc);
    public  String  cmodificarEmple(EmpleadoDao miEmpleM);
    public EmpleadoDao deliminarEmple(int  doc);
}
