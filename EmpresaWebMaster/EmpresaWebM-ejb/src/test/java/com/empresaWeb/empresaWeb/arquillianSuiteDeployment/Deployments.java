
package com.empresaWeb.empresaWeb.arquillianSuiteDeployment;

import java.io.File;
import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;

/**
 *
 * @author Administrator
 */
@ArquillianSuiteDeployment
public class Deployments {

    private static final String TEST_CLASSES_PATH = "./target/test-classes";
    private static final String BUSINESS_ARTIFACT_ID = "com.empresaWeb:EmpresaWebM-ejb:1.0";
    private static final String BUSINESS_MODULE = "EmpresaWebM-ejb-1.0.jar";
    


    @Deployment(name="epw")
    public static Archive<?> createDeployment() {

        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml");
        File[] libs = pom.importDependencies(ScopeType.PROVIDED).resolve().withTransitivity().asFile();
        File business = pom.resolve(BUSINESS_ARTIFACT_ID).withoutTransitivity().asSingleFile();
        
        EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class, "EmpresaWebM-ear.ear")
                .addAsLibraries(libs)
                .addAsModule(business, BUSINESS_MODULE);

        ear.getAsType(JavaArchive.class, BUSINESS_MODULE).as(ExplodedImporter.class)
                .importDirectory((new File(TEST_CLASSES_PATH)));
        System.out.println(ear.toString(true));
        return ear;
    }
    
    
}
