
package com.empresaWeb.empresaWeb.ejb;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Administrator
 */

public class MyBeanLocalTest extends Arquillian{
    @EJB(mappedName="MyBean#com.empresaWeb.empresaWeb.ejb.MyBeanLocal")
            MyBeanLocal myBeanL;
    
  /*  public void lookup() {
        try {
            InitialContext ctx = new InitialContext();
            myBeanL = (MyBeanLocal) ctx.lookup("MyBean#com.empresaWeb.empresaWeb.ejb.MyBeanLocal");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }*/
    
    @Test
    @OperateOnDeployment("epw")
    @Transactional(TransactionMode.ROLLBACK)
    public void aregistrarEmpleTest(){
    EmpleadoDao myEmployee = new EmpleadoDao();
    System.out.println("---------------------------");
    System.out.println("testing registrarEmple");
    System.out.println("---------------------------");
    try {
    
    myEmployee.setDocEmpleado1(Integer.parseInt("1234567"));
    myEmployee.setNombre1("Juan Luis");
    myEmployee.setApellido1("Castro Valencia");
    myEmployee.setDireccion1("Barrio Libertadores");
    myEmployee.setTelefono1("98765");
    myEmployee.setJefeInmediato1("Ivan Martinez");
    myEmployee.setIdDepartamento1(Integer.parseInt("1"));
    myEmployee.setIdContrato1(Integer.parseInt("2"));
    
    myBeanL.aregistrarEmple(myEmployee);
    
    } catch (Exception e) {
    e.printStackTrace();
    System.out.println(e.getMessage());
    Assert.fail("Error ejecutando el m�todo registrarEmple " + e.getMessage());
    }
    
    }
    @Test
    @OperateOnDeployment("epw")
    public  void bconsultarEmpleadoTest() {
        
        System.out.println("---------------------------");
        System.out.println("testing consultar Empleado");
        System.out.println("---------------------------");
        
        try {
            int doc= 1234567;
            
            EmpleadoDao    miemDao=myBeanL.bconsultar(doc);
            System.out.println("---------------------------");
            System.out.println(miemDao.getDocEmpleado1());
            System.out.println(miemDao.getNombre1());
            System.out.println(miemDao.getApellido1());
            System.out.println(miemDao.getDireccion1());
            System.out.println(miemDao.getTelefono1());
            System.out.println(miemDao.getJefeInmediato1());
            System.out.println(miemDao.getIdDepartamento1());
            System.out.println(miemDao.getIdContrato1());
            System.out.println("---------------------------");
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            Assert.fail("Error ejecutando el m�todo consultar " + e.getMessage());
        }
    }
    
    
    @Test
    @OperateOnDeployment("epw")
    @Transactional(TransactionMode.ROLLBACK)
    public void cmodificarTest(){
        System.out.println("---------------------------");
        System.out.println("testing Modificar Empleado");
        System.out.println("---------------------------");
        
        try {
            int doc= 1234567;
            EmpleadoDao empleadoMod = myBeanL.bconsultar(doc);
            System.out.println("------------datos Consultados---------------");
            System.out.println(empleadoMod.getDocEmpleado1());
            System.out.println(empleadoMod.getNombre1());
            System.out.println(empleadoMod.getApellido1());
            System.out.println(empleadoMod.getDireccion1());
            System.out.println(empleadoMod.getTelefono1());
            System.out.println(empleadoMod.getJefeInmediato1());
            System.out.println(empleadoMod.getIdDepartamento1());
            System.out.println(empleadoMod.getIdContrato1());
            
            System.out.println("------------datos a MODIFICAR---------------");
            empleadoMod.setDocEmpleado1(doc);
            System.out.println(empleadoMod.getDocEmpleado1());
            empleadoMod.setNombre1("Jose Luis");
            System.out.println(empleadoMod.getNombre1());
            empleadoMod.setApellido1("Romero");
            System.out.println(empleadoMod.getApellido1());
            empleadoMod.setDireccion1("terminal");
            System.out.println(empleadoMod.getDireccion1());
            empleadoMod.setTelefono1("3154570024");
            System.out.println(empleadoMod.getTelefono1());
            empleadoMod.setJefeInmediato1("IVAN");
            System.out.println(empleadoMod.getJefeInmediato1());
            empleadoMod.setIdDepartamento1(Integer.parseInt("2"));
            System.out.println(empleadoMod.getIdDepartamento1());
            empleadoMod.setIdContrato1(Integer.parseInt("2"));
            System.out.println(empleadoMod.getIdContrato1());
            System.out.println("-------------end--------------");
            myBeanL.cmodificarEmple(empleadoMod);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            Assert.fail("Error ejecutando el m�todo registrarEmple " + e.getMessage());
        }
    }
    
    
    @Test
    @OperateOnDeployment("epw")
    @Transactional(TransactionMode.ROLLBACK)
    public void deliminarEmple(){
        System.out.println("---------------------------");
        System.out.println("testing Eliminar Empleado");
        System.out.println("---------------------------");
        
        try {
            int doc= 1234567;
            EmpleadoDao empleEliminar = myBeanL.bconsultar(doc);
            empleEliminar.getDocEmpleado1();
            System.out.println(empleEliminar.getDocEmpleado1());
            myBeanL.deliminarEmple(doc);
           
        }  catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            Assert.fail("Error ejecutando el m�todo ELMINAR " + e.getMessage());
        }
        
    }
}
